"""this file is used to resample to fetched weather data required for linear regression analysis"""
"""for simplicity, only focuses on buildings located near Helsinki region"""

import numpy as np
import math
from datetime import datetime
import time
import requests
import matplotlib.pyplot as plt

import pandas as pd

from sklearn import datasets, linear_model


irradianceList = []
timeList = []
f = open('sunGlobalIrradianceVantaa.csv')
for line in f.readlines():

        timeValue, irradiance = line.split(';')
        irradiance = float(irradiance)
        #print(irradiance)
        timeValue = timeValue.rstrip()
        if(irradiance < 0): #0 is minimum, other is considered error
            irradiance = 0
        irradianceList.append(float(irradiance))
        #print(timeValue)
        timeValue = datetime.strptime(timeValue, "%Y-%m-%dT%H:%M:%SZ")
        #print(timeValue)
        timeList.append(timeValue)

        df = pd.DataFrame()
        df['timestamps'] = timeList
        se = pd.Series(irradianceList)
        df['irradiance']= se.values

df = df.set_index('timestamps')
df_avg = df.resample('3H').mean()

df_avg.to_csv('solarIrradianceResampled.csv', encoding='utf-8', index=True)
df_avg.iloc[-8:].to_csv('solarIrradianceResampledTomorrow.csv', encoding='utf-8', index=True)  #simulating this day as today and next one as tomorrow
df_avg.iloc[-16:-8].to_csv('solarIrradiancedResampledToday.csv', encoding='utf-8', index=True)
plt.figure()
df_avg.plot()
print(df_avg.head())

irradianceList = []
timeList = []
f = open('sunGlobalIrradianceVantaaWorst.csv')
for line in f.readlines():

        timeValue, irradiance = line.split(';')
        irradiance = float(irradiance)
        #print(irradiance)
        timeValue = timeValue.rstrip()
        if(irradiance < 0): #0 is minimum, other is considered error
            irradiance = 0
        irradianceList.append(float(irradiance))
        #print(timeValue)
        timeValue = datetime.strptime(timeValue, "%Y-%m-%dT%H:%M:%SZ")
        #print(timeValue)
        timeList.append(timeValue)

        df = pd.DataFrame()
        df['timestamps'] = timeList
        se = pd.Series(irradianceList)
        df['irradiance']= se.values

df = df.set_index('timestamps')
df_avg = df.resample('3H').mean()

df_avg.to_csv('solarIrradianceResampledWorst.csv', encoding='utf-8', index=True)
plt.figure()
df_avg.plot()
print(df_avg.head())

f = open('temperatureVantaa.csv')
temperatureList = []
timeList = []
for line in f.readlines():

        timeValue, temperature = line.split(';')
        temperature = float(temperature)
        timeValue = timeValue.rstrip()
        temperatureList.append(float(temperature))
        #print(timeValue)
        timeValue = datetime.strptime(timeValue, "%Y-%m-%dT%H:%M:%SZ")
        #print(timeValue)
        timeList.append(timeValue)

        df = pd.DataFrame()
        df['timestamps'] = timeList
        #print(len(timeList))
        se = pd.Series(temperatureList)
        #print(len(se.values))
        df['temperature']= se.values

df = df.set_index('timestamps')
df_avg = df.resample('3H').mean()

df_avg.to_csv('temperatureResampled.csv', encoding='utf-8', index=True)
df_avg.iloc[-8:].to_csv('temperatureResampledTomorrow.csv', encoding='utf-8', index=True)  #simulating this day as today and next one as tomorrow
df_avg.iloc[-16:-8].to_csv('temperatureResampledToday.csv', encoding='utf-8', index=True)
print(df_avg.head())

#plt.plot(powerList)
df_avg.plot()

f = open('temperatureVantaaWorst.csv')
temperatureList = []
timeList = []
for line in f.readlines():

        timeValue, temperature = line.split(';')
        temperature = float(temperature)
        timeValue = timeValue.rstrip()
        temperatureList.append(float(temperature))
        #print(timeValue)
        timeValue = datetime.strptime(timeValue, "%Y-%m-%dT%H:%M:%SZ")
        #print(timeValue)
        timeList.append(timeValue)

        df = pd.DataFrame()
        df['timestamps'] = timeList
        #print(len(timeList))
        se = pd.Series(temperatureList)
        #print(len(se.values))
        df['temperature']= se.values

df = df.set_index('timestamps')
df_avg = df.resample('3H').mean()

df_avg.to_csv('temperatureResampledWorst.csv', encoding='utf-8', index=True)
print(df_avg.head())

#plt.plot(powerList)
df_avg.plot()

f = open('windspeedVantaa.csv')
windspeedList = []
timeList = []
for line in f.readlines():

        timeValue, windspeed = line.split(';')
        windspeed = float(windspeed)
        timeValue = timeValue.rstrip()
        windspeedList.append(float(windspeed))
        #print(timeValue)
        timeValue = datetime.strptime(timeValue, "%Y-%m-%dT%H:%M:%SZ")
        #print(timeValue)
        timeList.append(timeValue)

        df = pd.DataFrame()
        df['timestamps'] = timeList
        #print(len(timeList))
        se = pd.Series(windspeedList)
        #print(len(se.values))
        df['windspeed']= se.values

df = df.set_index('timestamps')
df_avg = df.resample('3H').mean()

df_avg.to_csv('windspeedResampled.csv', encoding='utf-8', index=True)
df_avg.iloc[-8:].to_csv('windspeedResampledTomorrow.csv', encoding='utf-8', index=True)  #simulating this day as today and next one as tomorrow
df_avg.iloc[-16:-8].to_csv('windspeedResampledToday.csv', encoding='utf-8', index=True)
print(df_avg.head())
df_avg.plot()

f = open('windspeedVantaaWorst.csv')
windspeedList = []
timeList = []
for line in f.readlines():

        timeValue, windspeed = line.split(';')
        windspeed = float(windspeed)
        timeValue = timeValue.rstrip()
        windspeedList.append(float(windspeed))
        #print(timeValue)
        timeValue = datetime.strptime(timeValue, "%Y-%m-%dT%H:%M:%SZ")
        #print(timeValue)
        timeList.append(timeValue)

        df = pd.DataFrame()
        df['timestamps'] = timeList
        #print(len(timeList))
        se = pd.Series(windspeedList)
        #print(len(se.values))
        df['windspeed']= se.values

df = df.set_index('timestamps')
df_avg = df.resample('3H').mean()

df_avg.to_csv('windspeedResampledWorst.csv', encoding='utf-8', index=True)
print(df_avg.head())
df_avg.plot()


plt.show()
