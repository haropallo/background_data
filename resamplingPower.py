"""this file is used for resampling the high-resolution power values into 3H averages for the use of the app"""

import numpy as np
import math
from datetime import datetime
import time
import requests
import matplotlib.pyplot as plt

import pandas as pd

from sklearn import datasets, linear_model


# Create linear regression object
regr = linear_model.LinearRegression()

powerList = []
timeList = []
for locationId in range(1,24):
    if(locationId == 12):  #this location has some issue
        continue
    f = open('building' + str(locationId) + '.csv')
    for line in f.readlines():
            if "p" in line:
                continue
            timeValue, powerValue = line.split(';')
            powerValue = float(powerValue)
            #print()
            timeValue = timeValue.rstrip()
            if(powerValue > 20000): #suggested by challenge organizers to remove errors
                powerValue = 1
            powerList.append(float(powerValue))
            #print(timeValue)
            timeValue = datetime.strptime(timeValue, "%Y-%m-%d %H:%M:%S")
            #print(timeValue)
            timeList.append(timeValue)

    df = pd.DataFrame()
    df['timestamps'] = timeList
    se = pd.Series(powerList)
    df['power'] = se.values

    df = df.set_index('timestamps')
    df_avg = df.resample('3H').mean()

    df_avg.to_csv('building' + str(locationId) + 'Resampled.csv', encoding='utf-8', index=True)
    print(df_avg.head())






plt.figure()
#plt.plot(powerList)
df_avg.plot()
plt.show()
