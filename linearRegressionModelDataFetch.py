"""this file is used to fetch the weather data required for linear regression analysis (especially with )"""
"""for simplicity, only focuses on buildings located near Helsinki region"""

import numpy as np
import math
import datetime
import requests
import matplotlib.pyplot as plt

from sklearn import datasets, linear_model


startdate = datetime.date(2017,10,1) #sample measurement data starts
theEndDate = datetime.date(2017,11,16)  #sample measurement data ends
#print((theEndDate-startdate).days)

numberOfDays = (theEndDate-startdate).days

sunListTime = []
sunListValue = []
temperatureListTime = []
temperatureListValue = []
windListTime = []
windListValue = []

"""the weather data corresponding to the power measurement time period"""
"""first sun radiation data is fetched"""
for x in range(math.ceil(numberOfDays/7)):  #due to API limitations, only around 7 days can be fetched at once
    starttime = str(startdate)  #start date of the next iteration round
    print(starttime)
    enddate = startdate + datetime.timedelta(days=6)  #end date of the next iteration round
    endtime = str(enddate)


    url = 'http://data.fmi.fi/fmi-apikey/your_api_key_here/wfs?request=getFeature&storedquery_id=fmi::observations::radiation::timevaluepair&starttime=' + starttime + 'T00:00:00Z&endtime=' + endtime + 'T23:00:00Z&timestep=60&'
    r = requests.get(url)
    f = open('sun' + starttime + '.xml', 'w')
    f.write(r.text)
    f.close()

    """then we store all the values into array, have to disgard most of the file contents as irrelevant"""
    f = open('sun' + starttime + '.xml')
    vantaa = False
    globaali = False
    #lets choose location for this data (example)
    for line in f.readlines():
        if 'Vantaa Helsinki-Vantaan lentoasema' in line:
            vantaa = True
        if(vantaa):
            if 'obs-obs-1-2-GLOB_1MIN' in line:
                globaali = True
            if globaali:
                if '<wml2:time>' in line:
                        line = line.lstrip()
                        line = line.rstrip()
                        line = line.replace('<wml2:time>', '')
                        line = line.replace('</wml2:time>', '')
                        sunListTime.append(line)

                if '<wml2:value>' in line:

                        line = line.lstrip()
                        line = line.rstrip()
                        line = line.replace('<wml2:value>', '')
                        line = line.replace('</wml2:value>', '')
                        sunListValue.append(line)

                if '/wfs:member' in line:
                        break
        startdate = enddate + datetime.timedelta(days=1)
        #close the file
        f.close()


"""then temperature and windspeed data is fetched"""
startdate = datetime.date(2017,10,1) #sample measurement data starts
for x in range(math.ceil(numberOfDays/7)):  #due to API limitations, only around 7 days can be fetched at once
    starttime = str(startdate)  #start date of the next iteration round
    print(starttime)
    enddate = startdate + datetime.timedelta(days=6)  #end date of the next iteration round
    endtime = str(enddate)


    url = 'http://data.fmi.fi/fmi-apikey/your_api_key_here/wfs?request=getFeature&storedquery_id=fmi::observations::weather::timevaluepair&parameters=temperature,ws&place=kirkkonummi&starttime=' + starttime + 'T00:00:00Z&endtime=' + endtime + 'T23:00:00Z&timestep=60&'
    r = requests.get(url)
    f = open('tempAndWind' + starttime + '.xml', 'w')
    f.write(r.text)
    f.close()

    """then we store all the values into array, have to disgard most of the file contents as irrelevant"""
    f = open('tempAndWind' + starttime + '.xml')
    temperature = False
    wind = False
    #lets choose location for this data (example)
    for line in f.readlines():
            if 'obs-obs-1-1-temperature' in line:
                temperature = True
            if temperature:
                if '<wml2:time>' in line:
                        line = line.lstrip()
                        line = line.rstrip()
                        line = line.replace('<wml2:time>', '')
                        line = line.replace('</wml2:time>', '')
                        temperatureListTime.append(line)

                if '<wml2:value>' in line:

                        line = line.lstrip()
                        line = line.rstrip()
                        line = line.replace('<wml2:value>', '')
                        line = line.replace('</wml2:value>', '')
                        temperatureListValue.append(line)

                if '/wfs:member' in line:
                        temperature = False

            if 'obs-obs-1-1-ws' in line:
                wind = True
            if wind:
                if '<wml2:time>' in line:
                        line = line.lstrip()
                        line = line.rstrip()
                        line = line.replace('<wml2:time>', '')
                        line = line.replace('</wml2:time>', '')
                        windListTime.append(line)

                if '<wml2:value>' in line:

                        line = line.lstrip()
                        line = line.rstrip()
                        line = line.replace('<wml2:value>', '')
                        line = line.replace('</wml2:value>', '')
                        windListValue.append(line)

                if '/wfs:member' in line:
                    break

    startdate = enddate + datetime.timedelta(days=1)
    #close the file
    f.close()





#write all the solar data into one file
f = open('sunGlobalIrradianceVantaa.csv', 'w')
for i in range(len(sunListTime)):
    f.write(str(sunListTime[i]) + ';' + str(sunListValue[i]) + "\n")
f.close()

#write all the temperature data into one file
f = open('temperatureVantaa.csv', 'w')
for i in range(len(temperatureListTime)):
    f.write(str(temperatureListTime[i]) + ';' + str(temperatureListValue[i]) + "\n")
f.close()

#write all the windspeed data into one file
f = open('windspeedVantaa.csv', 'w')
for i in range(len(windListTime)):
    f.write(str(windListTime[i]) + ';' + str(windListValue[i]) + "\n")
f.close()

"""then let's get the wors case sample day (cold) also displayed in the app"""
sunListTime = []
sunListValue = []
temperatureListTime = []
temperatureListValue = []
windListTime = []
windListValue = []

startdate = datetime.date(2016,1,21) #sample measurement data starts, cold day!
"""first sun radiation data is fetched"""
for x in range(1):
    starttime = str(startdate)  #start date of the next iteration round
    endtime = starttime

    url = 'http://data.fmi.fi/fmi-apikey/your_api_key_here/wfs?request=getFeature&storedquery_id=fmi::observations::radiation::timevaluepair&starttime=' + starttime + 'T00:00:00Z&endtime=' + endtime + 'T23:00:00Z&timestep=60&'
    r = requests.get(url)
    f = open('sun' + starttime + '.xml', 'w')
    f.write(r.text)
    f.close()

    """then we store all the values into array, have to disgard most of the file contents as irrelevant"""
    f = open('sun' + starttime + '.xml')
    vantaa = False
    globaali = False
    #lets choose location for this data (example)
    for line in f.readlines():
        if 'Vantaa Helsinki-Vantaan lentoasema' in line:
            vantaa = True
        if(vantaa):
            if 'obs-obs-1-2-GLOB_1MIN' in line:
                globaali = True
            if globaali:
                if '<wml2:time>' in line:
                        line = line.lstrip()
                        line = line.rstrip()
                        line = line.replace('<wml2:time>', '')
                        line = line.replace('</wml2:time>', '')
                        sunListTime.append(line)

                if '<wml2:value>' in line:

                        line = line.lstrip()
                        line = line.rstrip()
                        line = line.replace('<wml2:value>', '')
                        line = line.replace('</wml2:value>', '')
                        sunListValue.append(line)

                if '/wfs:member' in line:
                        break
        #close the file
        f.close()


"""then temperature and windspeed data is fetched"""
for x in range(1):
    starttime = str(startdate)  #start date of the next iteration round
    endtime = starttime

    url = 'http://data.fmi.fi/fmi-apikey/2cf849b3-baa0-4db5-9146-02f4be5920e2/wfs?request=getFeature&storedquery_id=fmi::observations::weather::timevaluepair&parameters=temperature,ws&place=kirkkonummi&starttime=' + starttime + 'T00:00:00Z&endtime=' + endtime + 'T23:00:00Z&timestep=60&'
    r = requests.get(url)
    f = open('tempAndWind' + starttime + '.xml', 'w')
    f.write(r.text)
    f.close()

    """then we store all the values into array, have to disgard most of the file contents as irrelevant"""
    f = open('tempAndWind' + starttime + '.xml')
    temperature = False
    wind = False
    #lets choose location for this data (example)
    for line in f.readlines():
            if 'obs-obs-1-1-temperature' in line:
                temperature = True
            if temperature:
                if '<wml2:time>' in line:
                        line = line.lstrip()
                        line = line.rstrip()
                        line = line.replace('<wml2:time>', '')
                        line = line.replace('</wml2:time>', '')
                        temperatureListTime.append(line)

                if '<wml2:value>' in line:

                        line = line.lstrip()
                        line = line.rstrip()
                        line = line.replace('<wml2:value>', '')
                        line = line.replace('</wml2:value>', '')
                        temperatureListValue.append(line)

                if '/wfs:member' in line:
                        temperature = False

            if 'obs-obs-1-1-ws' in line:
                wind = True
            if wind:
                if '<wml2:time>' in line:
                        line = line.lstrip()
                        line = line.rstrip()
                        line = line.replace('<wml2:time>', '')
                        line = line.replace('</wml2:time>', '')
                        windListTime.append(line)

                if '<wml2:value>' in line:

                        line = line.lstrip()
                        line = line.rstrip()
                        line = line.replace('<wml2:value>', '')
                        line = line.replace('</wml2:value>', '')
                        windListValue.append(line)

                if '/wfs:member' in line:
                    break

    #close the file
    f.close()



#write all the solar data into one file
f = open('sunGlobalIrradianceVantaaWorst.csv', 'w')
for i in range(len(sunListTime)):
    f.write(str(sunListTime[i]) + ';' + str(sunListValue[i]) + "\n")
f.close()

#write all the temperature data into one file
f = open('temperatureVantaaWorst.csv', 'w')
for i in range(len(temperatureListTime)):
    f.write(str(temperatureListTime[i]) + ';' + str(temperatureListValue[i]) + "\n")
f.close()

#write all the windspeed data into one file
f = open('windspeedVantaaWorst.csv', 'w')
for i in range(len(windListTime)):
    f.write(str(windListTime[i]) + ';' + str(windListValue[i]) + "\n")
f.close()

plt.figure()
plt.plot(temperatureList)
plt.show()
