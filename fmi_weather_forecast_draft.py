"""unfinished draft of fetching weather forecast for the app"""

import matplotlib.pyplot as plt
import numpy as np
import xml.etree.ElementTree as ET
import requests
import math
import datetime

from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score



# Load the diabetes dataset
diabetes = datasets.load_diabetes()

startdate = datetime.date(2016,1,1)
startdateOriginal = str(startdate)

list0_0 = []
list0_1 = []
list1_0 = []
list1_1 = []
list2_0 = []
list2_1 = []

lista = []
lista1 = []
lista2 = []
powerList = []

for x in range(1):
    starttime = str(startdate)
    print(starttime)
    enddate = startdate + datetime.timedelta(days=6)
    endtime = str(enddate)
    """done"""
    url = 'http://data.fmi.fi/fmi-apikey/your_api_key_here/wfs?request=getFeature&storedquery_id=fmi::forecast::hirlam::surface::obsstations::timevaluepair&place=helsinki&'
    r = requests.get(url)
    print("done")
    f = open('weatherForecast' + starttime + '.xml', 'w')
    f.write(r.text)
    f.close()


    if 1 == 1:

        f = open('weatherForecast' + starttime + '.xml')


        #temperature = True
        vantaa = False
        temperature = False

        # for-loop käy jokaisen rivin läpi tiedostossa f.
        for line in f.readlines():
            if 'Helsinki-Vantaa' in line:
                vantaa = True
            if(vantaa):
                if 'mts-1-65-Temperature' in line:
                    temperature = True
                if temperature:
                    if '<wml2:time>' in line:
                        line = line.lstrip()
                        line = line.rstrip()
                        line = line.replace('<wml2:time>', '')
                        line = line.replace('</wml2:time>', '')
                        #print(line)
                        list2_0.append(line)
                    # Tiedostossa <gml:doubleOrNilReasonTupleList>  on se kohta, missä haluttu data sijaitsee. Silloin kun tämä
                    # tagi on käsittelyssä ollaan oikeassa kohtaa, ja nämä tiedot halutaan talteen.
                    if '<wml2:value>' in line:

                        line = line.lstrip()
                        line = line.rstrip()
                        line = line.replace('<wml2:value>', '')
                        line = line.replace('</wml2:value>', '')
                        list2_1.append(line)
                        #print(line)

                    if '/wfs:member' in line:
                        break

        f.close()

    f = open('temperatureForecast.csv', 'w')
    for item in list2_1:
        f.write(str(item) + "\n")
    f.close()
