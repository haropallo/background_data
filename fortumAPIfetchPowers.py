"""file for downloading the power profiles for the 23 buildings"""

import matplotlib.pyplot as plt
import numpy as np
import xml.etree.ElementTree as ET
import requests, json
import math
import datetime

password = '****'

base_url = 'https://fortum.hackjunction.com'

data = json.dumps({'password': password})

headers = {

 'content-type': 'application/json',

}



for locationId in range(1,24):  

    response = requests.post(base_url + '/api/locations/' + str(locationId), data, headers=headers)
    responseAsText = response.text

    responseAsText = responseAsText.replace('\r', '')

    f = open('building' + str(locationId)  + '.csv', 'w')
    f.write(responseAsText)
    f.close()
