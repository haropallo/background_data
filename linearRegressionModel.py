"""this file trains and fits linear regression models for the example buildings for three example days used in the app"""

import matplotlib.pyplot as plt
import numpy as np
import xml.etree.ElementTree as ET
import requests
import math
import datetime

from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score


temperatureList = []  #for training the regression model
windspeedList = []
irradianceList = []

temperatureListToday = []
windspeedListToday = []
irradianceListToday = []

temperatureListTomorrow = []
windspeedListTomorrow = []
irradianceListTomorrow = []

temperatureListWorst = []  #cold, high-price day
windspeedListWorst = []
irradianceListWorst = []

powerList = []

f = open('solarIrradianceResampledWorst.csv')
for line in f.readlines():
        timeValue, irradiance = line.split(',')
        if 'timestamps' in line:
            continue
        #irradiance = irradiance.rsplit()
        print(irradiance)
        irradiance = float(irradiance)
        irradianceListWorst.append(irradiance)

f = open('temperatureResampledWorst.csv')
for line in f.readlines():
        timeValue, temperature = line.split(',')
        if 'timestamps' in line:
            continue
        #temperature = temperature.rsplit()
        temperature = float(temperature)
        temperatureListWorst.append(temperature)

f = open('windspeedResampledWorst.csv')
for line in f.readlines():
        timeValue, windspeed = line.split(',')
        if 'timestamps' in line:
            continue
        #windspeed = windspeed.rsplit()
        windspeed = float(windspeed)
        windspeedListWorst.append(windspeed)


f = open('solarIrradianceResampledTomorrow.csv')
for line in f.readlines():
        timeValue, irradiance = line.split(',')
        if 'timestamps' in line:
            continue
        #irradiance = irradiance.rsplit()
        print(irradiance)
        irradiance = float(irradiance)
        irradianceListTomorrow.append(irradiance)

f = open('temperatureResampledTomorrow.csv')
for line in f.readlines():
        timeValue, temperature = line.split(',')
        if 'timestamps' in line:
            continue
        #temperature = temperature.rsplit()
        temperature = float(temperature)
        temperatureListTomorrow.append(temperature)

f = open('windspeedResampledTomorrow.csv')
for line in f.readlines():
        timeValue, windspeed = line.split(',')
        if 'timestamps' in line:
            continue
        #windspeed = windspeed.rsplit()
        windspeed = float(windspeed)
        windspeedListTomorrow.append(windspeed)

f = open('solarIrradianceResampledTomorrow.csv')
for line in f.readlines():
        timeValue, irradiance = line.split(',')
        if 'timestamps' in line:
            continue
        #irradiance = irradiance.rsplit()
        print(irradiance)
        irradiance = float(irradiance)
        irradianceListToday.append(irradiance)

f = open('temperatureResampledToday.csv')
for line in f.readlines():
        timeValue, temperature = line.split(',')
        if 'timestamps' in line:
            continue
        #temperature = temperature.rsplit()
        temperature = float(temperature)
        temperatureListToday.append(temperature)

f = open('windspeedResampledToday.csv')
for line in f.readlines():
        timeValue, windspeed = line.split(',')
        if 'timestamps' in line:
            continue
        #windspeed = windspeed.rsplit()
        windspeed = float(windspeed)
        windspeedListToday.append(windspeed)

f = open('solarIrradianceResampled.csv')
for line in f.readlines():
        timeValue, irradiance = line.split(',')
        if 'timestamps' in line:
            continue
        #irradiance = irradiance.rsplit()
        print(irradiance)
        irradiance = float(irradiance)
        irradianceList.append(irradiance)

f = open('temperatureResampled.csv')
for line in f.readlines():
        timeValue, temperature = line.split(',')
        if 'timestamps' in line:
            continue
        #temperature = temperature.rsplit()
        temperature = float(temperature)
        temperatureList.append(temperature)

f = open('windspeedResampled.csv')
for line in f.readlines():
        timeValue, windspeed = line.split(',')
        if 'timestamps' in line:
            continue
        #windspeed = windspeed.rsplit()
        windspeed = float(windspeed)
        windspeedList.append(windspeed)

for locationId in [1,11,17]:
    powerList = []
    #f = open('building19Resampled.csv')
    f = open('building' + str(locationId) + 'Resampled.csv')
    for line in f.readlines():
            timeValue, power = line.split(',')
            if 'timestamps' in line:
                continue
            #power = power.rsplit()
            print(power)
            power = float(power)
            powerList.append(power)


    timeOfDayArrayNight = [1,1,0,0,0,0,0,0] * 365
    timeOfDayArrayMorning = [0,0,1,1,0,0,0,0] * 365
    timeOfDayArrayDay = [0,0,0,0,1,1,0,0] * 365
    timeOfDayArrayEvening = [0,0,0,0,0,0,1,1] * 365

    timeOfDayArrayNight = np.asarray(timeOfDayArrayNight).reshape(-1,1)
    timeOfDayArrayMorning = np.asarray(timeOfDayArrayMorning).reshape(-1,1)
    timeOfDayArrayDay = np.asarray(timeOfDayArrayDay).reshape(-1,1)
    timeOfDayArrayEvening = np.asarray(timeOfDayArrayEvening).reshape(-1,1)

    #print(timeOfDayArrayNight)

    timeOfDayArray = np.concatenate((timeOfDayArrayNight,timeOfDayArrayMorning), axis=1)
    timeOfDayArray = np.concatenate((timeOfDayArray,timeOfDayArrayDay), axis=1)
    timeOfDayArray = np.concatenate((timeOfDayArray,timeOfDayArrayEvening), axis=1)

    timeOfDayArrayOneDay = timeOfDayArray[:8,:]

    #timeOfDayArray = timeOfDayArray.reshape(4,1)
    print(timeOfDayArrayOneDay)


    #60% point for training
    lenghtOfPower = len(powerList)
    sixtyPoint = int(np.ceil(lenghtOfPower*0.6))
    eightyPoint = int(np.ceil(lenghtOfPower*0.8))
    print(sixtyPoint)

    powerList = np.asarray(powerList)
    windspeedList = np.asarray(windspeedList)
    irradianceList = np.asarray(irradianceList)
    temperatureList = np.asarray(temperatureList)

    powerList_Y_train = powerList[1:sixtyPoint]
    powerList_Y_train = powerList_Y_train.reshape(-1,1)
    powerList_Y_test = powerList[sixtyPoint:eightyPoint]
    powerList_Y_test = powerList_Y_test.reshape(-1,1)

    timeOfDayArray_X_train = timeOfDayArray[1:sixtyPoint, :]
    timeOfDayArray_X_test = timeOfDayArray[sixtyPoint:eightyPoint, :]

    temperature_X_train = temperatureList[1:sixtyPoint]
    temperature_X_train = temperature_X_train.reshape(-1,1)
    temperature_X_test = temperatureList[sixtyPoint:eightyPoint]
    temperature_X_test = temperature_X_test.reshape(-1,1)

    temperature_X_train_lag = temperatureList[:sixtyPoint-1]
    temperature_X_train_lag = temperature_X_train_lag.reshape(-1,1)
    temperature_X_test_lag = temperatureList[sixtyPoint-1:eightyPoint-1]
    temperature_X_test_lag = temperature_X_test_lag.reshape(-1,1)

    windspeed_X_train = windspeedList[1:sixtyPoint]
    windspeed_X_test = windspeedList[sixtyPoint:eightyPoint]
    windspeed_X_train = windspeed_X_train.reshape(-1,1)
    windspeed_X_test = windspeed_X_test.reshape(-1,1)

    windspeed_X_train_lag = windspeedList[:sixtyPoint-1]
    windspeed_X_test_lag = windspeedList[sixtyPoint-1:eightyPoint-1]
    windspeed_X_train_lag = windspeed_X_train_lag.reshape(-1,1)
    windspeed_X_test_lag = windspeed_X_test_lag.reshape(-1,1)

    solarRad_X_train = irradianceList[1:sixtyPoint]
    solarRad_X_test = irradianceList[sixtyPoint:eightyPoint]
    solarRad_X_train = solarRad_X_train.reshape(-1,1)
    solarRad_X_test = solarRad_X_test.reshape(-1,1)

    solarRad_X_train_lag = irradianceList[:sixtyPoint-1]
    solarRad_X_test_lag = irradianceList[sixtyPoint-1:eightyPoint-1]
    solarRad_X_train_lag = solarRad_X_train.reshape(-1,1)
    solarRad_X_test_lag = solarRad_X_test.reshape(-1,1)



    weather_X_train = np.concatenate((temperature_X_train, windspeed_X_train), axis = 1)
    weather_X_train = np.concatenate((weather_X_train, solarRad_X_train), axis = 1)
    weather_X_test = np.concatenate((temperature_X_test, windspeed_X_test), axis = 1)
    weather_X_test = np.concatenate((weather_X_test, solarRad_X_test), axis = 1)
    #add 6h lag
    weather_X_train_lag = np.concatenate((temperature_X_train_lag, windspeed_X_train_lag), axis = 1)
    weather_X_train_lag = np.concatenate((weather_X_train_lag, solarRad_X_train_lag), axis = 1)

    weather_X_test_lag = np.concatenate((temperature_X_test_lag, windspeed_X_test_lag), axis = 1)
    weather_X_test_lag = np.concatenate((weather_X_test_lag, solarRad_X_test_lag), axis = 1)

    weather_X_train_total = np.concatenate((weather_X_train, weather_X_train_lag), axis =1)
    weather_X_test_total = np.concatenate((weather_X_test, weather_X_test_lag), axis =1)


    weather_and_timeOfDay_X_train = np.concatenate((np.multiply(weather_X_train[:,0].reshape(-1,1), timeOfDayArray_X_train), np.multiply(weather_X_train[:,1].reshape(-1,1), timeOfDayArray_X_train)), axis=1)
    weather_and_timeOfDay_X_train = np.concatenate((weather_and_timeOfDay_X_train, np.multiply(weather_X_train[:,2].reshape(-1,1), timeOfDayArray_X_train)), axis=1)

    weather_and_timeOfDay_X_test = np.concatenate((np.multiply(weather_X_test[:,0].reshape(-1,1), timeOfDayArray_X_test), np.multiply(weather_X_test[:,1].reshape(-1,1), timeOfDayArray_X_test)), axis=1)
    weather_and_timeOfDay_X_test = np.concatenate((weather_and_timeOfDay_X_test, np.multiply(weather_X_test[:,2].reshape(-1,1), timeOfDayArray_X_test)), axis=1)

    print(timeOfDayArrayOneDay)
    print(temperatureListToday)
    weather_and_timeOfDay_X_forecast = np.concatenate((np.multiply(timeOfDayArrayOneDay, np.asarray(temperatureListToday).reshape(-1,1)), np.multiply(timeOfDayArrayOneDay, np.asarray(windspeedListToday).reshape(-1,1))), axis=1)
    weather_and_timeOfDay_X_forecast = np.concatenate((weather_and_timeOfDay_X_forecast, np.multiply(timeOfDayArrayOneDay, np.asarray(irradianceListToday).reshape(-1,1))), axis=1)
    print(weather_and_timeOfDay_X_forecast)

    weather_and_timeOfDay_X_forecastTomorrow = np.concatenate((np.multiply(timeOfDayArrayOneDay, np.asarray(temperatureListTomorrow).reshape(-1,1)), np.multiply(timeOfDayArrayOneDay, np.asarray(windspeedListTomorrow).reshape(-1,1))), axis=1)
    weather_and_timeOfDay_X_forecastTomorrow = np.concatenate((weather_and_timeOfDay_X_forecastTomorrow, np.multiply(timeOfDayArrayOneDay, np.asarray(irradianceListTomorrow).reshape(-1,1))), axis=1)
    print(weather_and_timeOfDay_X_forecastTomorrow)

    weather_and_timeOfDay_X_forecastWorst = np.concatenate((np.multiply(timeOfDayArrayOneDay, np.asarray(temperatureListWorst).reshape(-1,1)), np.multiply(timeOfDayArrayOneDay, np.asarray(windspeedListWorst).reshape(-1,1))), axis=1)
    weather_and_timeOfDay_X_forecastWorst = np.concatenate((weather_and_timeOfDay_X_forecastWorst, np.multiply(timeOfDayArrayOneDay, np.asarray(irradianceListWorst).reshape(-1,1))), axis=1)
    print(weather_and_timeOfDay_X_forecastWorst)

    # Create linear regression object
    regr = linear_model.LinearRegression()

    #print(len(temperature_X_train), len(powerList_Y_train))
    # Train the model using the training sets
    #regr.fit(temperature_X_train, powerList_Y_train)
    #regr.fit(weather_X_train, powerList_Y_train)
    #regr.fit(weather_X_train_total, powerList_Y_train)
    regr.fit(weather_and_timeOfDay_X_train, powerList_Y_train)

    print(regr.get_params)
    # Make predictions using the testing set
    power_y_pred = []
    #for x in temperature_X_test:
    #for x in weather_X_test:
    #for x in weather_X_test_total:
    for x in weather_and_timeOfDay_X_test:
        #power_y_pred.append(float(regr.predict(x.reshape(1,-1))))
        #power_y_pred.append(float(regr.predict(x.reshape(1,3))))
        #power_y_pred.append(float(regr.predict(x.reshape(1,6))))
        power_y_pred.append(float(regr.predict(x.reshape(1,12))))

    power_y_pred = np.asarray(power_y_pred)
    print(power_y_pred)

    power_y_forecast = []
    for x in weather_and_timeOfDay_X_forecast:  #17.11.2017
        power_y_forecast.append(float(regr.predict(x.reshape(1,12))))

    power_y_forecast = np.asarray(power_y_forecast)
    power_y_forecast = np.multiply(power_y_forecast, [1,1,0.7,0.7,0.7,0.7,0.7,0.7])  #removing consumption (1-0.7) which will be added in the app
    print(power_y_forecast)


    print('Coefficients: \n', regr.coef_)


    f = open('powerTodayBuilding'+str(locationId)+'.csv', 'w')
    for value in power_y_forecast:
        f.write(str(value) + '\n')
    f.close()



    power_y_forecast = []
    for x in weather_and_timeOfDay_X_forecastTomorrow:  #17.11.2017
        power_y_forecast.append(float(regr.predict(x.reshape(1,12))))

    power_y_forecast = np.asarray(power_y_forecast)
    power_y_forecast = np.multiply(power_y_forecast, [1,1,0.7,0.7,0.7,0.7,0.7,0.7])  #removing consumption (1-0.7) which will be added in the app as dispatchable loads

    f = open('powerTomorrowBuilding'+ str(locationId) + '.csv', 'w')
    for value in power_y_forecast:
        f.write(str(value) + '\n')
    f.close()
    # Plot outputs
    plt.plot(power_y_forecast)




    power_y_forecast = []
    for x in weather_and_timeOfDay_X_forecastWorst:  #17.11.2017
        power_y_forecast.append(float(regr.predict(x.reshape(1,12))))

    power_y_forecast = np.asarray(power_y_forecast)
    power_y_forecast = np.multiply(power_y_forecast, [1,1,0.7,0.7,0.7,0.7,0.7,0.7])  #removing consumption (1-0.7) which will be added in the app as dispatchable loads

    f = open('powerWorstBuilding'+ str(locationId) + '.csv', 'w')
    for value in power_y_forecast:
        f.write(str(value) + '\n')
    f.close()

    # Plot outputs
    plt.plot(powerList_Y_test,  color='black')
    plt.plot(power_y_pred, color='blue', linewidth=3)
    plt.plot(power_y_forecast)

plt.xticks(())
plt.yticks(())

plt.show()
